import * as url from "url";
import { AxiosRequestConfig, AxiosResponse } from "axios"
import { crudApi } from "./httpClient"

/**
 * Strictly typed API Client Factories
 */


export class AnalyticsApi {
    private config: Omit<AxiosRequestConfig, "method"> = {}

    constructor(config?: Omit<AxiosRequestConfig, "method">) {
        this.config = config || {}
    }

    public async visualizeEventSegmentationChart(): Promise<AxiosResponse<EventSegmentationChartResponse>> {
        // request has no params
        const baseUrl = `/v1/analytics/event-segmentation`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), ...this.config })
    }
    public async visualizeFunnel(): Promise<AxiosResponse<Array<FunnelResponse>>> {
        // request has no params
        const baseUrl = `/v1/analytics/funnel`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), ...this.config })
    }
    public async visualizeRetentionChart(): Promise<AxiosResponse<RetentionChartResponse>> {
        // request has no params
        const baseUrl = `/v1/analytics/retention`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), ...this.config })
    }
}


export class AuthenticationApi {
    private config: Omit<AxiosRequestConfig, "method"> = {}

    constructor(config?: Omit<AxiosRequestConfig, "method">) {
        this.config = config || {}
    }

    public async createTenant(params: {  body: CreateTenantBody; }): Promise<AxiosResponse<SignupResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/auth/create-tenant`;
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "POST", url: url.format(urlObj), data, ...this.config })
    }
    public async generateApiKey(): Promise<AxiosResponse<GenerateApiKeyResponse>> {
        // request has no params
        const baseUrl = `/v1/auth/api-key`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "PUT", url: url.format(urlObj), ...this.config })
    }
    public async getExistsApiKey(): Promise<AxiosResponse<ExistsApiKeyResponse>> {
        // request has no params
        const baseUrl = `/v1/auth/api-key`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), ...this.config })
    }
    public async invite(params: {  body: InviteUserBody; }): Promise<AxiosResponse<InviteUserResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/auth/invite`;
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "POST", url: url.format(urlObj), data, ...this.config })
    }
    public async login(params: {  body: LoginBody; }): Promise<AxiosResponse<LoginResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/auth/login`;
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "POST", url: url.format(urlObj), data, ...this.config })
    }
    public async reauthenticate(): Promise<AxiosResponse<LoginResponse>> {
        // request has no params
        const baseUrl = `/v1/auth/reauthenticate`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "POST", url: url.format(urlObj), ...this.config })
    }
    public async signup(params: {  body: SignupBody; }): Promise<AxiosResponse<SignupResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/auth/signup`;
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "POST", url: url.format(urlObj), data, ...this.config })
    }
    public async verifyInvitationToken(params: {  body: VerifyInvitationTokenBody; }): Promise<AxiosResponse<VerifyInvitationTokenResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/auth/invite/verify`;
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "POST", url: url.format(urlObj), data, ...this.config })
    }
}


export class ChartsApi {
    private config: Omit<AxiosRequestConfig, "method"> = {}

    constructor(config?: Omit<AxiosRequestConfig, "method">) {
        this.config = config || {}
    }

    public async findChartById(params: {  id: string; }): Promise<AxiosResponse<ChartResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/charts/{id}`
            .replace("{ id }".replace(/[ .*]/g, ""), `${params.id}`);
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), data, ...this.config })
    }
    public async postChart(params: {  body: ChartBody; }): Promise<AxiosResponse<Array<ChartResponse>>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/charts`;
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "POST", url: url.format(urlObj), data, ...this.config })
    }
    public async updateChartById(params: {  id: string;  body: PatchChartBody; }): Promise<AxiosResponse<ChartResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/charts/{id}`
            .replace("{ id }".replace(/[ .*]/g, ""), `${params.id}`);
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "PATCH", url: url.format(urlObj), data, ...this.config })
    }
}


export class DashboardsApi {
    private config: Omit<AxiosRequestConfig, "method"> = {}

    constructor(config?: Omit<AxiosRequestConfig, "method">) {
        this.config = config || {}
    }

    public async createDashboard(params: {  body: CreateDashboardBody; }): Promise<AxiosResponse<DashboardResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/dashboards`;
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "POST", url: url.format(urlObj), data, ...this.config })
    }
    public async getAllDashboards(): Promise<AxiosResponse<Array<DashboardResponse>>> {
        // request has no params
        const baseUrl = `/v1/dashboards`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), ...this.config })
    }
    public async getChartsByDashboardId(params: {  id: string; }): Promise<AxiosResponse<Array<ChartResponse>>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/dashboards/{id}/charts`
            .replace("{ id }".replace(/[ .*]/g, ""), `${params.id}`);
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), data, ...this.config })
    }
    public async updateDashboard(params: {  id: string;  body: UpdateDashboardBody; }): Promise<AxiosResponse<DashboardResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/dashboards/{id}`
            .replace("{ id }".replace(/[ .*]/g, ""), `${params.id}`);
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "PUT", url: url.format(urlObj), data, ...this.config })
    }
}


export class SdkApi {
    private config: Omit<AxiosRequestConfig, "method"> = {}

    constructor(config?: Omit<AxiosRequestConfig, "method">) {
        this.config = config || {}
    }

    public async postTrackedTenant(params: {  body: TrackedTenantSdkBody; }): Promise<AxiosResponse<InsertOrUpdateTrackedTenantSdkResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/track/tenants`;
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "POST", url: url.format(urlObj), data, ...this.config })
    }
    public async postTrackedUser(params: {  body: TrackedUserSdkBody; }): Promise<AxiosResponse<InsertOrUpdateTrackedUserSdkResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/track/users`;
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "POST", url: url.format(urlObj), data, ...this.config })
    }
    public async postTrakedEvent(params: {  body: CreateOrUpdateEventSdkBody; }): Promise<AxiosResponse<CreateOrUpdateEventSdkResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/track/events`;
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "POST", url: url.format(urlObj), data, ...this.config })
    }
}


export class TrackedEventsApi {
    private config: Omit<AxiosRequestConfig, "method"> = {}

    constructor(config?: Omit<AxiosRequestConfig, "method">) {
        this.config = config || {}
    }

    public async getEventHistory(): Promise<AxiosResponse<Array<TrackedEventsResponse>>> {
        // request has no params
        const baseUrl = `/v1/tracked-events`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), ...this.config })
    }
}


export class TrackedUsersApi {
    private config: Omit<AxiosRequestConfig, "method"> = {}

    constructor(config?: Omit<AxiosRequestConfig, "method">) {
        this.config = config || {}
    }

    public async getTrackedUser(params: {  id: string; }): Promise<AxiosResponse<TrackedUserResponse>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/tracked-users/{id}`
            .replace("{ id }".replace(/[ .*]/g, ""), `${params.id}`);
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), data, ...this.config })
    }
}


export class UniqueEventsApi {
    private config: Omit<AxiosRequestConfig, "method"> = {}

    constructor(config?: Omit<AxiosRequestConfig, "method">) {
        this.config = config || {}
    }

    public async getEvents(): Promise<AxiosResponse<Array<UniqueEventResponse>>> {
        // request has no params
        const baseUrl = `/v1/events`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), ...this.config })
    }
}


export class UniquePropertiesApi {
    private config: Omit<AxiosRequestConfig, "method"> = {}

    constructor(config?: Omit<AxiosRequestConfig, "method">) {
        this.config = config || {}
    }

    public async getUniqueEventProperties(): Promise<AxiosResponse<Array<UniqueEventPropertiesResponse>>> {
        // request has no params
        const baseUrl = `/v1/properties/events`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), ...this.config })
    }
    public async getUniqueSessionProperties(): Promise<AxiosResponse<Array<UniqueSessionPropertiesResponse>>> {
        // request has no params
        const baseUrl = `/v1/properties/sessions`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), ...this.config })
    }
    public async getUniqueTenantProperties(): Promise<AxiosResponse<Array<UniqueTenantPropertiesResponse>>> {
        // request has no params
        const baseUrl = `/v1/properties/tenants`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), ...this.config })
    }
    public async getUniqueUserProperties(): Promise<AxiosResponse<Array<UniqueUserPropertiesResponse>>> {
        // request has no params
        const baseUrl = `/v1/properties/users`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), ...this.config })
    }
}


export class UsersApi {
    private config: Omit<AxiosRequestConfig, "method"> = {}

    constructor(config?: Omit<AxiosRequestConfig, "method">) {
        this.config = config || {}
    }

    public async findAllUsers(): Promise<AxiosResponse<Array<UsersResponse>>> {
        // request has no params
        const baseUrl = `/v1/users`;
        const urlObj = url.parse(baseUrl, true);
        return await crudApi({ method: "GET", url: url.format(urlObj), ...this.config })
    }
    public async updateUsers(params: {  body: Array<UserBody>; }): Promise<AxiosResponse<Array<UsersResponse>>> {
        let data = {} // eslint-disable-line
        const baseUrl = `/v1/users`;
        const urlObj = url.parse(baseUrl, true);
        // body params
        this.config = {
            ...this.config,
            headers: {
                ...this.config.headers,
                "Content-Type": "application/json"
            }
        };
        if (params["body"]) {
            data = JSON.stringify(params["body"] || {});
        }
        return await crudApi({ method: "PUT", url: url.format(urlObj), data, ...this.config })
    }
}


/**
 * Types
 */

interface ChartBody {
    "dashboardId"?: string;
    "type": string;
    "name": string;
    "description"?: string;
    "query": any;
}


interface ChartResponse {
    "id": string;
    "dashboardId"?: string;
    "type": string;
    "name": string;
    "description"?: string;
    "query": any;
    "data": ChartResponseData;
}


interface ChartResponseData {
    "options": any;
    "data": Array<any>;
}


interface CreateDashboardBody {
    "name": string;
    "isPrimary": boolean;
    "description"?: string;
}


interface CreateOrUpdateEventSdkBody {
    "internalTenantId"?: string;
    "internalUserId": string;
    "tenantId"?: string;
    "userId": string;
    "name": string;
    "properties"?: any;
    "session": SessionSdkBody;
}


interface CreateOrUpdateEventSdkResponse {
    "session": SessionSdkResponse;
}


interface CreateTenantBody {
    "companyName": string;
    "email": string;
    "password": string;
    "firstName": string;
    "lastName": string;
}


interface DashboardResponse {
    "id": string;
    "name": string;
    "isPrimary": boolean;
    "description"?: string;
    "chartSettings": any;
}


interface EventSegmentationChartData {
    "date": string;
    "meta": any;
}


interface EventSegmentationChartOptions {
    "visualizationType": string;
    "intervalUnit": string;
    "startDate": string;
    "endDate": string;
}


interface EventSegmentationChartResponse {
    "options": EventSegmentationChartOptions;
    "data": Array<EventSegmentationChartData>;
}


interface ExistsApiKeyResponse {
    "existsApiKey": boolean;
}


interface FunnelMeta {
    "userIds": Array<string>;
}


interface FunnelResponse {
    "eventName": string;
    "eventCount": number;
    "meta": FunnelMeta;
}


interface GenerateApiKeyResponse {
    "apiKey": string;
}


interface InsertOrUpdateTrackedTenantSdkResponse {
    "internalTenantId"?: string;
}


interface InsertOrUpdateTrackedUserSdkResponse {
    "internalUserId": string;
    "sessionId": string;
    "sessionUpdatedAt": string;
}


interface InviteUserBody {
    "senderName": string;
    "email": string;
}


interface InviteUserResponse {
    "success": boolean;
    "message": string;
}


interface LoginBody {
    "username": string;
    "password": string;
}


interface LoginResponse {
    "token": string;
    "user": UserResponse;
}


interface PatchChartBody {
    "dashboardId"?: string;
    "name"?: string;
    "description"?: string;
    "query"?: any;
}


interface RetentionChartResponse {
    "options": RetentionChartResponseOptions;
    "cohorts": Array<RetentionChartResponseCohorts>;
}


interface RetentionChartResponseCohortData {
    "intervalsFromStart": number;
    "retainedUserCount": number;
    "meta": RetentionChartResponseCohortDataMeta;
}


interface RetentionChartResponseCohortDataMeta {
    "userIds": Array<string>;
}


interface RetentionChartResponseCohorts {
    "cohortStartDate": string;
    "cohortSize": number;
    "data": Array<RetentionChartResponseCohortData>;
}


interface RetentionChartResponseOptions {
    "intervalUnit": string;
    "startDate": string;
    "endDate": string;
}


interface SessionSdkBody {
    "sessionId"?: string;
    "os"?: string;
    "osVersion"?: string;
    "browser"?: string;
    "browserVersion"?: string;
    "country"?: string;
    "region"?: string;
}


interface SessionSdkResponse {
    "id": string;
    "updatedAt": string;
}


interface SignupBody {
    "tenantId": string;
    "email": string;
    "password": string;
    "firstName": string;
    "lastName": string;
}


interface SignupResponse {
    "statusCode": number;
    "message": string;
}


interface TrackedEventsResponse {
    "id": string;
    "trackedUserId": string;
    "name": string;
    "createdAt": string;
}


interface TrackedTenantSdkBody {
    "id": string;
    "companyName": string;
    "properties"?: any;
}


interface TrackedUserEventsResponse {
    "id": string;
    "name": string;
    "properties": any;
    "createdAt": string;
}


interface TrackedUserResponse {
    "id": string;
    "externalId": string;
    "firstName": string;
    "lastName": string;
    "email": string;
    "properties": any;
    "createdAt": string;
    "updatedAt": string;
    "trackedTenant": TrackedUserTenantResponse;
    "trackedSessions": Array<TrackedUserSessionResponse>;
}


interface TrackedUserSdkBody {
    "internalTenantId"?: string;
    "id": string;
    "tenantId"?: string;
    "firstName"?: string;
    "lastName"?: string;
    "email"?: string;
    "session"?: SessionSdkBody;
    "properties"?: any;
}


interface TrackedUserSessionResponse {
    "id": string;
    "os": string;
    "osVersion": string;
    "browser": string;
    "browserVersion": string;
    "country": string;
    "region": string;
    "createdAt": string;
    "updatedAt": string;
    "trackedEvents": Array<TrackedUserEventsResponse>;
}


interface TrackedUserTenantResponse {
    "id": string;
    "externalId": string;
    "companyName": string;
    "properties": any;
}


interface UniqueEventPropertiesResponse {
    "id": string;
    "type": string;
    "property": string;
    "values"?: Array<string>;
}


interface UniqueEventResponse {
    "id": string;
    "name": string;
    "description"?: string;
}


interface UniqueSessionPropertiesResponse {
    "id": string;
    "property": string;
    "values": Array<string>;
}


interface UniqueTenantPropertiesResponse {
    "id": string;
    "type": string;
    "property": string;
    "values"?: Array<string>;
}


interface UniqueUserPropertiesResponse {
    "id": string;
    "type": string;
    "property": string;
    "values"?: Array<string>;
}


interface UpdateDashboardBody {
    "name": string;
    "isPrimary": boolean;
    "isPrimaryChanged": boolean;
    "description"?: string;
    "chartSettings": any;
    "removedChartIds"?: Array<string>;
}


interface UserBody {
    "id": string;
    "firstName": string;
    "lastName": string;
    "email": string;
    "permission": number;
}


interface UserResponse {
    "id": string;
    "email": string;
    "firstName": string;
    "lastName": string;
    "avatar"?: string;
    "tenantId": string;
    "companyName": string;
    "permission": number;
    "plan": number;
    "createdAt": string;
    "updatedAt": string;
}


interface UsersResponse {
    "id": string;
    "firstName": string;
    "lastName": string;
    "email": string;
    "permission": number;
}


interface VerifyInvitationTokenBody {
    "token": string;
}


interface VerifyInvitationTokenResponse {
    "isValid": boolean;
    "tenantId"?: string;
    "companyName"?: string;
    "email"?: string;
}


