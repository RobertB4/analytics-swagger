/**
 * This file lists all API request/response types
 */

export interface ChartBody {
    "dashboardId"?: string;
    "type": string;
    "name": string;
    "description"?: string;
    "query": any;
}


export interface ChartResponse {
    "id": string;
    "dashboardId"?: string;
    "type": string;
    "name": string;
    "description"?: string;
    "query": any;
    "data": ChartResponseData;
}


export interface ChartResponseData {
    "options": any;
    "data": Array<any>;
}


export interface CreateDashboardBody {
    "name": string;
    "isPrimary": boolean;
    "description"?: string;
}


export interface CreateOrUpdateEventSdkBody {
    "internalTenantId"?: string;
    "internalUserId": string;
    "tenantId"?: string;
    "userId": string;
    "name": string;
    "properties"?: any;
    "session": SessionSdkBody;
}


export interface CreateOrUpdateEventSdkResponse {
    "session": SessionSdkResponse;
}


export interface CreateTenantBody {
    "companyName": string;
    "email": string;
    "password": string;
    "firstName": string;
    "lastName": string;
}


export interface DashboardResponse {
    "id": string;
    "name": string;
    "isPrimary": boolean;
    "description"?: string;
    "chartSettings": any;
}


export interface EventSegmentationChartData {
    "date": string;
    "meta": any;
}


export interface EventSegmentationChartOptions {
    "visualizationType": string;
    "intervalUnit": string;
    "startDate": string;
    "endDate": string;
}


export interface EventSegmentationChartResponse {
    "options": EventSegmentationChartOptions;
    "data": Array<EventSegmentationChartData>;
}


export interface ExistsApiKeyResponse {
    "existsApiKey": boolean;
}


export interface FunnelMeta {
    "userIds": Array<string>;
}


export interface FunnelResponse {
    "eventName": string;
    "eventCount": number;
    "meta": FunnelMeta;
}


export interface GenerateApiKeyResponse {
    "apiKey": string;
}


export interface InsertOrUpdateTrackedTenantSdkResponse {
    "internalTenantId"?: string;
}


export interface InsertOrUpdateTrackedUserSdkResponse {
    "internalUserId": string;
    "sessionId": string;
    "sessionUpdatedAt": string;
}


export interface InviteUserBody {
    "senderName": string;
    "email": string;
}


export interface InviteUserResponse {
    "success": boolean;
    "message": string;
}


export interface LoginBody {
    "username": string;
    "password": string;
}


export interface LoginResponse {
    "token": string;
    "user": UserResponse;
}


export interface PatchChartBody {
    "dashboardId"?: string;
    "name"?: string;
    "description"?: string;
    "query"?: any;
}


export interface RetentionChartResponse {
    "options": RetentionChartResponseOptions;
    "cohorts": Array<RetentionChartResponseCohorts>;
}


export interface RetentionChartResponseCohortData {
    "intervalsFromStart": number;
    "retainedUserCount": number;
    "meta": RetentionChartResponseCohortDataMeta;
}


export interface RetentionChartResponseCohortDataMeta {
    "userIds": Array<string>;
}


export interface RetentionChartResponseCohorts {
    "cohortStartDate": string;
    "cohortSize": number;
    "data": Array<RetentionChartResponseCohortData>;
}


export interface RetentionChartResponseOptions {
    "intervalUnit": string;
    "startDate": string;
    "endDate": string;
}


export interface SessionSdkBody {
    "sessionId"?: string;
    "os"?: string;
    "osVersion"?: string;
    "browser"?: string;
    "browserVersion"?: string;
    "country"?: string;
    "region"?: string;
}


export interface SessionSdkResponse {
    "id": string;
    "updatedAt": string;
}


export interface SignupBody {
    "tenantId": string;
    "email": string;
    "password": string;
    "firstName": string;
    "lastName": string;
}


export interface SignupResponse {
    "statusCode": number;
    "message": string;
}


export interface TrackedEventsResponse {
    "id": string;
    "trackedUserId": string;
    "name": string;
    "createdAt": string;
}


export interface TrackedTenantSdkBody {
    "id": string;
    "companyName": string;
    "properties"?: any;
}


export interface TrackedUserEventsResponse {
    "id": string;
    "name": string;
    "properties": any;
    "createdAt": string;
}


export interface TrackedUserResponse {
    "id": string;
    "externalId": string;
    "firstName": string;
    "lastName": string;
    "email": string;
    "properties": any;
    "createdAt": string;
    "updatedAt": string;
    "trackedTenant": TrackedUserTenantResponse;
    "trackedSessions": Array<TrackedUserSessionResponse>;
}


export interface TrackedUserSdkBody {
    "internalTenantId"?: string;
    "id": string;
    "tenantId"?: string;
    "firstName"?: string;
    "lastName"?: string;
    "email"?: string;
    "session"?: SessionSdkBody;
    "properties"?: any;
}


export interface TrackedUserSessionResponse {
    "id": string;
    "os": string;
    "osVersion": string;
    "browser": string;
    "browserVersion": string;
    "country": string;
    "region": string;
    "createdAt": string;
    "updatedAt": string;
    "trackedEvents": Array<TrackedUserEventsResponse>;
}


export interface TrackedUserTenantResponse {
    "id": string;
    "externalId": string;
    "companyName": string;
    "properties": any;
}


export interface UniqueEventPropertiesResponse {
    "id": string;
    "type": string;
    "property": string;
    "values"?: Array<string>;
}


export interface UniqueEventResponse {
    "id": string;
    "name": string;
    "description"?: string;
}


export interface UniqueSessionPropertiesResponse {
    "id": string;
    "property": string;
    "values": Array<string>;
}


export interface UniqueTenantPropertiesResponse {
    "id": string;
    "type": string;
    "property": string;
    "values"?: Array<string>;
}


export interface UniqueUserPropertiesResponse {
    "id": string;
    "type": string;
    "property": string;
    "values"?: Array<string>;
}


export interface UpdateDashboardBody {
    "name": string;
    "isPrimary": boolean;
    "isPrimaryChanged": boolean;
    "description"?: string;
    "chartSettings": any;
    "removedChartIds"?: Array<string>;
}


export interface UserBody {
    "id": string;
    "firstName": string;
    "lastName": string;
    "email": string;
    "permission": number;
}


export interface UserResponse {
    "id": string;
    "email": string;
    "firstName": string;
    "lastName": string;
    "avatar"?: string;
    "tenantId": string;
    "companyName": string;
    "permission": number;
    "plan": number;
    "createdAt": string;
    "updatedAt": string;
}


export interface UsersResponse {
    "id": string;
    "firstName": string;
    "lastName": string;
    "email": string;
    "permission": number;
}


export interface VerifyInvitationTokenBody {
    "token": string;
}


export interface VerifyInvitationTokenResponse {
    "isValid": boolean;
    "tenantId"?: string;
    "companyName"?: string;
    "email"?: string;
}


